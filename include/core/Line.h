//
//  Line.h
//  Paint
//
//  Created by Joseph Chow on 8/13/15.
//
//

#ifndef __Paint__Line__
#define __Paint__Line__

#include "cinder/gl/GlslProg.h"
#include "cinder/gl/VboMesh.h"
#include "cinder/BSpline.h"
#include "cinder/gl/Fbo.h"
#include <vector>


//! This class forms the basis for creating user drawable lines
namespace xoio {
    
    class Line {
        
        //! shader for rendering the line
        ci::gl::GlslProgRef renderShader;
        
        //! points to make up the final mesh of the line
        std::vector<ci::vec3> points;
        
        //! degree for the spline
        int degree = 3;
        
        //! splines
        ci::BSpline3f spline;
        
        //! fbo
        ci::gl::FboRef fbo;
        
        //! mesh for the things
        ci::gl::VboMeshRef mesh;
        
        //! batch to draw
        cinder::gl::BatchRef batch;
        
        //! layout for the mesh
        std::vector<ci::gl::VboMesh::Layout> bufferLayout;
        
    public:
        Line();
        
        void prepare();
        void update();
        void draw();
        
        void renderToFbo();
        void buildMesh();
        
        //! adds a position to the line
        void addPoint(ci::vec3 position);
        
        void buildPoints(std::vector<ci::vec3> points);
        
        
    };

}
#endif /* defined(__Paint__Line__) */
