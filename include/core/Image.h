//
//  Image.h
//  GenerativeImagry
//
//  Created by Joseph Chow on 9/10/15.
//
//

#ifndef __GenerativeImagry__Image__
#define __GenerativeImagry__Image__
#include "cinder/gl/Texture.h"
#include "cinder/ImageIo.h"
#include <vector>

namespace xoio {
    class ImageAsset {
        
        //! surface used to hold image
        ci::Surface32fRef surface;
        
        //! colors of the image
        std::vector<ci::vec4> imageColors;
        
        //! flag to indicate whether or not the image has loaded
        bool loadedImage = false;
        
        //! the threshold for what determines a black pixel
        int blackThreshold = 50;
        
        //! the threshold for what determines a white pixel
        int whiteThreshold = 200;
        
        std::vector<PixelInfo> pixelData;
        
    public:
        ImageAsset();
        
        //! loads the image
        void loadImage(std::string path);
        
        //! returns the image as a TextureRef
        ci::gl::TextureRef getTexture();
        
        //! breaks a image down into a vector of data based on the color of each pixel
        void breakdownImage();
        
        //! works like ImageAsset::breakdownImage but attempts to subtract all pixels that are either too black or too white
        void sampleNonGrayscaleColors();
        
        //! returns the width of the image
        int getWidth();
        
        //! returns the height
        int getHeight();
        
        //! returns the area
        int getArea();
        
        //! returns the colors of the image
        std::vector<ci::vec4> getColors();
        
        //! returns the current size of the sampled colors
        int getSize();
        
        ci::vec4 getColorAt(cinder::vec2 position);
        
    };
}

#endif /* defined(__GenerativeImagry__Image__) */
