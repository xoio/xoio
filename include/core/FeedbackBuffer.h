//
//  FeedbackBuffer.h
//  InterleaveTest
//
//  Created by Joseph Chow on 10/25/15.
//
//

#ifndef __InterleaveTest__FeedbackBuffer__
#define __InterleaveTest__FeedbackBuffer__

#include "cinder/gl/Vbo.h"
#include "cinder/gl/TransformFeedbackObj.h"

namespace xoio {
    
    
    /**
     *  A class to (hopefully) make it a bit simpler to use Transform feedback.
     *  Designed to either be subclassed or used on it's own.
     */
    class FeedbackBuffer {
        
        //! buffers for ping-ponging
        ci::gl::VboRef buffers[2];
        
        //! vaos to hold attributes;
        ci::gl::VaoRef vaos[2];
        
        //! transform feedback object for bouncing sets of vbos
        ci::gl::TransformFeedbackObjRef feedbackObjs[2];
        
    protected:
        //! The geometry type for doing transform feedback
        GLenum drawType = GL_POINTS;
        
        //! Indicates how your data is formed. Data is considered seperate by default
        GLenum dataStructureType = GL_SEPARATE_ATTRIBS;
        
        //! acts as a flag to indicate which in the set of buffers and vaos we're on
        uint32_t mDrawBuff = 1;
        
        int numItems = 0;
        
    public:
        FeedbackBuffer();
        
        
        //! shader used for updating
        ci::gl::GlslProgRef updateShader;
        
        //! shader used for rendering.
        ci::gl::GlslProgRef renderShader;
        
        //! retrives bothe of the buffers
        ci::gl::VboRef* getBuffers();
        
        void bind();
        void unbind();
        
        //! returns the current buffer we're drawing to
        ci::gl::VboRef getDrawBuffer(){
            return buffers[1-mDrawBuff];
        }
        
        template <typename T>
        void uniform(std::string name, T data);
        
        void update(const std::function<void()> fn);
        
        //! sets the type of structure your attributes should be considered in
        void setDataStructureType(GLenum dataStructureType=GL_SEPARATE_ATTRIBS);
        
        //! sets the geometry type used in transform feedback
        void setGeometryType(GLenum drawType=GL_POINTS);
        
        //! Applies data you set onto a buffer.
        //! Expects a callback function for enabling and pointing to vertex attributes.
        //! The structure of your data can be interleaved or not depending on what you're building.
        template<typename T>
        void setData(typename std::vector<T> &data,std::function<void()> fn){
            buffers[0] = ci::gl::Vbo::create( GL_ARRAY_BUFFER, data.size() * sizeof(T), data.data(), GL_STATIC_DRAW );
            // Create another Position Buffer that is null, for ping-ponging
            buffers[1] = ci::gl::Vbo::create( GL_ARRAY_BUFFER, data.size() * sizeof(T), nullptr, GL_STATIC_DRAW );
            
            numItems = data.size();
            for( int i = 0; i < 2; i++ ) {
                // Initialize the Vao's holding the info for each buffer
                vaos[i] = ci::gl::Vao::create();
                
                vaos[i]->bind();
                buffers[i]->bind();
                fn();
            }
            
        }
        
        //! Sets up the update shader. Takes a vector of varyings and attribute locations. For the attribute locations,
        //! the ordering is expected to line up with the index that that attribute is associated with, for instance, if
        //! a position attribute is supposed to be at index 0, that position attribute should be the first thing added to the
        //! vector you pass in.
        void setUpdateShader(std::string updateShaderPath,std::vector<std::string> varyings, std::vector<std::string> attribLocations);
        
        
        //! sets up the rendering shader. It's assumed that the shaders have the same name with different ending for each component.
        //! Vertex shaders have the .vert ending, etc.
        void setRenderingShader(std::string renderShaderPath, std::vector<std::string> attribLocations=std::vector<std::string>(0));
        
        void setUpdateShader(ci::gl::GlslProgRef updateShader);
        void setRenderShader(ci::gl::GlslProgRef renderShader);
    };

}
#endif /* defined(__InterleaveTest__FeedbackBuffer__) */
