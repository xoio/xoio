//
//  ParticleSystem.h
//  xoio
//
//  Created by Joseph Chow on 8/14/15.
//
//

#ifndef __xoio__ParticleSystem__
#define __xoio__ParticleSystem__

#include "cinder/gl/GlslProg.h"
#include <vector>

//! the base class which contains the components necessary for a transform feedback
//! powered particle systme
class ParticleSystem {
    
    //! render shader for the system
    ci::gl::GlslProgRef rendershader;

    //! particle positions
    std::vector<ci::vec3> positions;
    
    ///////// BUFFERS ///////////
    ci::gl::VboRef buffers[2];
    ci::gl::VaoRef vaos[2];
    int currentIndex = 0;
    
    
public:
    ParticleSystem();
    void setup();
};

#endif /* defined(__xoio__ParticleSystem__) */
