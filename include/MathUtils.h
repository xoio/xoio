//
//  MathUtils.h
//  TypeStudies
//
//  Created by Joseph Chow on 10/6/15.
//
//

#ifndef TypeStudies_MathUtils_h
#define TypeStudies_MathUtils_h
#include <random>
namespace xoio {
    namespace Math{
        
        template<class T>
        const T& constrain(const T& x, const T& a, const T& b){
            if(x < a){
                return a;
            }else if(b < x){
                return b;
            }else {
                return x;
            }
            
        }
        
        /**
         * Returns a random floating point number. Unlike ci::Rand , this will 
         * consistantly return different values each time.
         */
        static float random(int min, int max){
             std::random_device rd;
            std::mt19937 gen(rd());
            std::uniform_real_distribution<> dis(min,max);
            
            return dis(gen);
        }
        
        
        static float map(float val, float start1, float stop1, float start2, float stop2){
            float outgoing =
            start2 + (stop2 - start2) * ((val - start1) / (stop1 - start1));
            
            if(outgoing != outgoing){
                outgoing = 0.0;
                
#ifdef CINDER_MAC
#ifdef XOIO_DEBUG
       ci::app::console()<<"Flowfield::map : number is NaN\n";
#endif
#endif
                
            }else if(outgoing == std::numeric_limits<float>::infinity() || outgoing == !std::numeric_limits<float>::infinity()){
#ifdef CINDER_MAC
#ifdef XOIO_DEBUG
       ci::app::console()<<"Flowfield::map : number is inifity\n";          
#endif
#endif
            }
            
            return outgoing;

        }
    }
}

#endif
