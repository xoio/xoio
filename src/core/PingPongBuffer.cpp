//
//  PingPongBuffer.cpp
//  xoio
//
//  Created by Joseph Chow on 8/25/15.
//
//

#include "PingPongBuffer.h"

using namespace ci;
using namespace std;

namespace xoio {
    namespace core {
        PingPongBuffer::PingPongBuffer(){}
        PingPongBuffer::PingPongBuffer(int width, int height){
            this->width = width;
            this->height = height;
            
        }
        
        void PingPongBuffer::setup(){
            mCurrentFBO = 0;
            mOtherFBO = 1;
            mFBOs[0] = gl::Fbo::create( width,height, gl::Fbo::Format().colorTexture().disableDepth() );
            mFBOs[1] = gl::Fbo::create( width,height, gl::Fbo::Format().colorTexture().disableDepth()  );
        }
        
        void PingPongBuffer::update(){
            mCurrentFBO = ( mCurrentFBO + 1 ) % 2;
            mOtherFBO   = ( mCurrentFBO + 1 ) % 2;
        }
    }
}