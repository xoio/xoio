//
//  Image.cpp
//  xoio
//
//  Created by Joseph Chow on 9/10/15.
//
//

#include "Image.h"

using namespace std;
using namespace ci;

namespace xoio {
    ImageAsset::ImageAsset(){}
    void ImageAsset::loadImage(std::string path){
        bool assetFail = false;
        
        try {
            surface = Surface32f::create(ci::loadImage(app::loadAsset(path)));
        }catch(...){
            app::console()<<"Unable to load image " << path << " from assets folder. Trying in resources...\n";
            assetFail = true;
            
        }
        
        if (assetFail) {
            try {
                surface = Surface32f::create(ci::loadImage(app::loadResource(path)));
            }catch(...){
                app::console()<<"Unable to load image " << path << " from resources folder :(...\n";
            }
        }
        
        loadedImage = true;
    }
    
    gl::TextureRef ImageAsset::getTexture(){
        if(loadedImage){
            return gl::Texture::create(*surface);
        }
    }
    
    int ImageAsset::getSize(){
        if(imageColors.size() > 1){
            return imageColors.size();
        }
    }
    
    void ImageAsset::breakdownImage(){
        Surface32f::Iter it = surface->getIter();
        imageColors.clear();
        while (it.line()) {
            while(it.pixel()){
                vec4 color = vec4(it.r(),it.g(),it.b(),it.a());
                imageColors.push_back(color);
            }
        }
    }
    
    void ImageAsset::sampleNonGrayscaleColors(){
        Surface32f::Iter it = surface->getIter();
        imageColors.clear();
        while (it.line()) {
            while(it.pixel()){
                vec4 color = vec4();
                
                if (it.r() > blackThreshold && it.r() < whiteThreshold) {
                    color.x = it.r();
                }
                
                if (it.g() > blackThreshold && it.g() < whiteThreshold) {
                    color.x = it.g();
                }
                
                if (it.b() > blackThreshold && it.b() < whiteThreshold) {
                    color.x = it.b();
                }
                
                color.a = it.a();
                
                imageColors.push_back(color);
            }
        }
    }
    
    ci::vec4 ImageAsset::getColorAt(cinder::vec2 position){
        return surface->getPixel(position);
    }
    int ImageAsset::getWidth(){
        return surface->getWidth();
    }
    
    int ImageAsset::getHeight(){
        return surface->getHeight();
    }
    
    int ImageAsset::getArea(){
        return getWidth() * getHeight();
    }
    
    std::vector<ci::vec4> ImageAsset::getColors(){
        return imageColors;

    
}
