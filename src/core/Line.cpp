//
//  Line.cpp
//  xoio
//
//  Created by Joseph Chow on 8/13/15.
//
//

#include "core/Line.h"

using namespace ci;
using namespace std;

namespace xoio {
    
    Line::Line(){}
    
    void Line::addPoint(ci::vec3 position){
        points.push_back(position);
    }
    
    void Line::renderToFbo(){
        gl::ScopedFramebuffer fbscp(fbo);{
            gl::clear(Color(0,0,0));
            gl::ScopedViewport vp(ivec2(0),fbo->getSize());{
                
            }
        }
    }
    
    
    void Line::prepare(){
        
        
    }
    
    void Line::buildPoints(std::vector<ci::vec3> points){
        for(int i = 0; i < points.size();++i){
            this->points.push_back(points.at(i));
        }
    }
    
    void Line::buildMesh(){
        //setup the spline
        spline = BSpline3f(points,3,true,false);
        
        gl::Fbo::Format format;
        fbo = gl::Fbo::create(1024, 1024,format.depthTexture());
        
        bufferLayout = {
            gl::VboMesh::Layout().usage( GL_DYNAMIC_DRAW ).attrib( geom::Attrib::POSITION, 3 )
            //gl::VboMesh::Layout().usage( GL_STATIC_DRAW ).attrib( geom::Attrib::TEX_COORD_0, 2 )
        };
        
        mesh = gl::VboMesh::create( points.size(), GL_TRIANGLE_FAN , { bufferLayout } );
        mesh->bufferAttrib(geom::POSITION,points.size() * sizeof(vec3),points.data());
        
        renderShader = gl::getStockShader(gl::ShaderDef().color());
        
        batch = gl::Batch::create(mesh, renderShader);
        
        // mEdgesMesh->bufferAttrib( geom::POSITION, edgePositions.size() * sizeof( vec3 ), edgePositions.data() );
        // mEdgesMesh->bufferAttrib( geom::COLOR, edgeColors );
        // mVerticesMesh->bufferAttrib( geom::POSITION, vertexPositions.size() * sizeof( vec3 ), vertexPositions.data() );
        // mVerticesMesh->bufferAttrib( geom::COLOR, vertexColors );
        
    }
    
    void Line::update(){}
    void Line::draw(){
        batch->draw();
    }
}