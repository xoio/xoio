//
//  FeedbackBuffer.cpp
//  InterleaveTest
//
//  Created by Joseph Chow on 10/25/15.
//
//

#include "FeedbackBuffer.h"
using namespace ci;
using namespace std;
namespace xoio {
    
    FeedbackBuffer::FeedbackBuffer(){}
    
    void FeedbackBuffer::setUpdateShader(std::string updateShaderPath, std::vector<std::string> varyings, std::vector<std::string> attribLocations){
        
        gl::GlslProg::Format fmt;
        fmt.vertex(app::loadAsset(updateShaderPath));
        fmt.feedbackVaryings(varyings);
        
        for(int i = 0; i < attribLocations.size(); ++i){
            fmt.attribLocation(attribLocations.at(i), i);
        }
        
        updateShader = gl::GlslProg::create(fmt);
    }
    
    ci::gl::VboRef* FeedbackBuffer::getBuffers(){
        return buffers;
    }
    
    void FeedbackBuffer::setUpdateShader(ci::gl::GlslProgRef updateShader){
        this->updateShader = updateShader;
    }
    
    void FeedbackBuffer::setRenderShader(ci::gl::GlslProgRef renderShader){
        this->renderShader = renderShader;
    }
    
    
    void FeedbackBuffer::update(const std::function<void()> fn){
        // This equation just reliably swaps all concerned buffers
        mDrawBuff = 1 - mDrawBuff;
        
        gl::ScopedGlslProg	glslScope( updateShader );
        // We use this vao for input to the Glsl, while using the opposite
        // for the TransformFeedbackObj.
        gl::ScopedVao		vaoScope( vaos[mDrawBuff] );
        // Because we're not using a fragment shader, we need to
        // stop the rasterizer. This will make sure that OpenGL won't
        // move to the rasterization stage.
        gl::ScopedState		stateScope( GL_RASTERIZER_DISCARD, true );
        
        updateShader->uniform( "Time", app::getElapsedFrames() / 60.0f );
        
        // bind your buffer with the callback. Using callback for flexibility between interleaved and seperate attributes
        //gl::bindBufferBase( GL_TRANSFORM_FEEDBACK_BUFFER, 0, buffers[1-mDrawBuff] );
        fn();
        
        // We begin Transform Feedback, using the same primitive that
        // we're "drawing". Using points for the particle system.
        gl::beginTransformFeedback( drawType );
        gl::drawArrays( GL_POINTS, 0, numItems );
        gl::endTransformFeedback();
        
    }
    template<typename T>
    void FeedbackBuffer::uniform(std::string name, T data){
        
    }
    
    void FeedbackBuffer::setGeometryType(GLenum drawType){
        this->drawType = drawType;
    }
    
    void FeedbackBuffer::setDataStructureType(GLenum dataStructureType){
        this->dataStructureType = dataStructureType;
    }
    
    void FeedbackBuffer::bind(){
        
        
        gl::ScopedVao vao(vaos[1-mDrawBuff]);
        gl::ScopedGlslProg render(renderShader);
        gl::setDefaultShaderVars();
    }
    
    void FeedbackBuffer::unbind(){
    }
    
    void FeedbackBuffer::setRenderingShader(std::string renderShaderPath,std::vector<std::string> attribLocations){
        
        gl::GlslProg::Format fmt;
        fmt.vertex(app::loadAsset(renderShaderPath + ".vert"));
        fmt.fragment(app::loadAsset(renderShaderPath + ".frag"));
        
        if(attribLocations.size() > 1){
            for(int i = 0; i < attribLocations.size(); ++i){
                fmt.attribLocation(attribLocations.at(i), i);
            }
        }
        
        renderShader = gl::GlslProg::create(fmt);
        
    }
    
}