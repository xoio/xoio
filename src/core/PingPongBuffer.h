//
//  PingPongBuffer.h
//  xoio
//  Simple ping-pong buffer based on Robert Hodgin's Reaction Diffusion Cinder example
//  Created by Joseph Chow on 8/25/15.
//
//

#ifndef __xoio__PingPongBuffer__
#define __xoio__PingPongBuffer__

#include "cinder/gl/Fbo.h"

namespace xoio {
    namespace core {
        
        class PingPongBuffer {
            
            int width,height;
            int mCurrentFBO, mOtherFBO;
            ci::gl::FboRef mFBOs[2];

        public:
            PingPongBuffer();
            PingPongBuffer(int width,int height);
            void setup();
            void update();
        };
        
    }
}

#endif /* defined(__xoio__PingPongBuffer__) */
