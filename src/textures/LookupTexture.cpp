//
//  LookupTexture.cpp
//  TextureLookupTests
//
//  Created by Joseph Chow on 11/9/15.
//
//

#include "LookupTexture.h"
using namespace std;
using namespace ci;
using namespace app;

LookupTexture::LookupTexture(int resolution){
    this->resolution = resolution;
    loadShader();
}

LookupTexture::LookupTexture(){
    loadShader();
}


void LookupTexture::loadShader(){
    fragShader = STRINGIFY(
                           
                           uniform sampler2D fieldLookup;
                           uniform vec2 desiredPosition;
                           in vec2 TexCoord;
                           out vec4 oColor;
                           
                           void main()
                           {
                               vec2 resolution = vec2(640,480);
                               
                               
                               vec4 data = texture(fieldLookup,desiredPosition);
                               
                               //oColor = vec4(desiredPosition.xy,0.0,1.0);
                               oColor = vec4(data.xyz,1.0);
                               //oColor = vec4(1.0,1.0,0.0,1.0);
                               
                           });
    
    
    vertShader = STRINGIFY(
                           
                           uniform mat4	ciModelViewProjection;
                           uniform mat3	ciNormalMatrix;
                           
                           in vec4		ciPosition;
                           in vec2		ciTexCoord0;
                           in vec3		ciNormal;
                           in vec4		ciColor;
                           
                           out vec2	TexCoord;
                           out vec4	Color;
                           out vec3	Normal;
                           
                           void main( void )
                           {
                               gl_Position	= ciModelViewProjection * ciPosition;
                               Color 		= ciColor;
                               TexCoord	= ciTexCoord0;
                               Normal		= ciNormalMatrix * ciNormal;
                           }
                           );

}

void LookupTexture::setup(Surface32f data){
    
    //! setup texture format for lookup
    gl::Texture::Format texFmt;
    texFmt.setInternalFormat(GL_RGBA);
    texFmt.loadTopDown();
   
    //setup fbo
    gl::Fbo::Format bufferFmt;
    bufferFmt.setColorTextureFormat(texFmt);
  

    //initialize everything
    buffer = gl::Fbo::create(data.getWidth(),data.getHeight(),bufferFmt);
 
    width = data.getWidth();
    height = data.getHeight();
    
    gl::GlslProg::Format shdFmt;
    //shdFmt.vertex(loadAsset("passthru.glsl"));
    //shdFmt.fragment(loadAsset("mesh.frag"));
    shdFmt.vertex(vertShader);
    shdFmt.fragment(fragShader);
    shdFmt.version(150);
    
    lookupShader = gl::GlslProg::create(shdFmt);
    lookupTexture = gl::Texture::create(data);

  
    //gl::setMatricesWindow(app::getWindowSize());
    //TODO flip orientation to be top-left instead of bottom-left
    //write the lookup texture to the Fbo
    gl::ScopedFramebuffer fbo(buffer);{
        // clear out the FBO with blue
        gl::clear( Color( 0.25, 0.5f, 1.0f ) );
   
        gl::setMatricesWindow(buffer->getSize());
        gl::viewport( buffer->getSize());
        
        gl::ScopedGlslProg shd(lookupShader);
        //gl::ScopedTextureBind tex(lookupTexture);
        gl::drawSolidRect(Rectf(0,0,width,height));
    };
    
}

void LookupTexture::lookupPosition(ci::vec2 position){
    gl::ScopedFramebuffer fbo(buffer);{
        // clear out the FBO with blue
        gl::clear( Color( 0.25, 0.5f, 1.0f ) );
        
        gl::setMatricesWindow(buffer->getSize());
        gl::viewport( buffer->getSize());
        
        gl::ScopedTextureBind tex(lookupTexture,0);
        gl::ScopedGlslProg shd(lookupShader);
        
        lookupShader->uniform("fieldLookup",0);
        
        position.x *= resolution;
        position.y = (height * resolution) - position.y;
        position = glm::normalize(position);
       
        lookupShader->uniform("desiredPosition",position);
        gl::drawSolidRect(Rectf(0,0,width,height));
    };

}

gl::TextureRef LookupTexture::getPosition(){
    return buffer->getColorTexture();
}

gl::TextureRef LookupTexture::getPosition(ci::vec2 position){
    lookupPosition(position);
    return buffer->getColorTexture();
}