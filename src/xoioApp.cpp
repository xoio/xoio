#include "cinder/app/App.h"
#include "cinder/app/RendererGl.h"
#include "cinder/gl/gl.h"

using namespace ci;
using namespace ci::app;
using namespace std;

class xoioApp : public App {
  public:
	void setup() override;
	void mouseDown( MouseEvent event ) override;
	void update() override;
	void draw() override;
};

void xoioApp::setup()
{
}

void xoioApp::mouseDown( MouseEvent event )
{
}

void xoioApp::update()
{
}

void xoioApp::draw()
{
	gl::clear( Color( 0, 0, 0 ) ); 
}

CINDER_APP( xoioApp, RendererGl )
